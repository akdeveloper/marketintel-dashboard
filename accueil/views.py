#Importons les modules nécessaires
from django.shortcuts import render, redirect
from .forms import ConnexionForm, InscriptionForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User


"""
    Créons la vue qui permettra d'afficher dans un tableau toutes les lignes de notre BDD
"""
def accueil(request):
    return render(request, 'accueil/accueil.html')

"""
    Créons la vue pour le formulaire de connexion
"""
def connexion(request):
    #Chargeaons le formulaire de connexion
    form = ConnexionForm(request.POST)

    if form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]

        #Testons la connexion avec les informations de l'utilisateur
        user = authenticate(username = username, password = password)
        if user : 
            login(request, user)
            return render(request, 'accueil/accueil.html', locals())
        else:
            error = True
            return render(request, 'accueil/connexion.html', locals())
    else:
        form = ConnexionForm()

        return render(request, 'accueil/connexion.html', locals())

"""
    Créons la vue pour le formulaire d'Inscription
"""
def inscription(request):
    #Chargeaons le formulaire d'inscription
    form = InscriptionForm(request.POST)

    if form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        email = form.cleaned_data["email"]

        #Insérons un nouvel utilisateur dans la BDD à l'aide de la méthode create_user
        user = User.objects.create_user(username = username, email = email, password = password)
         
        user.first_name, user.last_name = first_name, last_name
        user.save()

        #Authentifions le nouvel utilisateur
        user = authenticate(username = username, password = password)
        login(request, user)

        return render(request, 'accueil/accueil.html', locals())
    else:
        form = InscriptionForm()

        return render(request, 'accueil/inscription.html', locals())