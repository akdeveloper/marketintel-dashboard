from django.db import models

class Offre(models.Model):
    intitule = models.CharField(max_length=100)
    description = models.TextField()
    secteur = models.TextField()
    salaire = models.TextField()
    typeContrat = models.TextField()
    experience = models.TextField()
    niveau = models.TextField()
    entreprise = models.TextField()
    date = models.TextField()
    lieu = models.TextField()
    pays = models.CharField(max_length=50)
    lien = models.CharField(max_length=255,primary_key=True)
