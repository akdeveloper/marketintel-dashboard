from django.urls import path
from emploisdashboard import views

urlpatterns = [
path('',views.index,name="indexemplois"),
path('pays/<p>/',views.pays,name="pays"),
path('secteur/<s>/',views.secteur,name="secteur"),
path('offre/<p>/<s>',views.pays_secteur,name="pays_secteur"),
path('rechercheInt',views.rechercheInt,name="rechercheInt"),
]
