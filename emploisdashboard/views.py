from django.shortcuts import render
from .models import Offre
from django.core.paginator import Paginator
from django.db.models import Count


#Accueil du dashboard offre d'emplois
def index(request):
    #la liste des offres
    offres_list = Offre.objects.all()
    paginator = Paginator(offres_list,6)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    #les statistiques générales
    nbreOffres = offres_list.count()

    nbrePays = Offre.objects.all().values_list('pays').distinct().count() - 1

    secteurs = ["informatique","Santé","Commerce","Gestion","Agriculture","Restauration","Transport","Justice","Administration","Environnement","Vente","Ressources Humaines","Industrie","Marketing","Services","Batiment"]
    ns = 0
    for sect in secteurs:
        i = Offre.objects.filter(secteur__contains=sect).count()
        if i!=0:
            ns = ns + 1

    #les statistiques sur les types de contrats
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #les statistiques sur les secteurs
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100,2)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100,2)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100,2)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100,2)

    nbreBanque = Offre.objects.filter(secteur__contains="Banque").count()
    pourcentageBanque = round(nbreBanque/nbreOffres*100,2)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100,2)

    return render(request,"index.html",{'offres': offres, 'pays': pays,'no': nbreOffres,'np': nbrePays,'ns': ns,'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'banque': pourcentageBanque,'env': pourcentageEnv})


#page filtre par pays
def pays(request,p):
    #la liste des offres filtrée par pays
    offres_list = Offre.objects.filter(pays=p)
    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    pays = p

    #les statistiques générales
    nbreOffres = offres_list.count()

    nbrePays = 1

    secteurs = ["informatique","Santé","Commerce","Gestion","Agriculture","Restauration","Transport","Justice","Administration","Environnement","Vente","Ressources Humaines","Industrie","Marketing","Services","Batiment"]
    ns = 0
    for sect in secteurs:
        i = Offre.objects.filter(secteur__contains="Informatique",pays=p).count()
        if i!=0:
            ns = ns + 1


    #les statistiques concernant les types de contrats
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(pays=p).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(pays=p).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(pays=p).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(pays=p).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(pays=p).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #les statistiques concernant les secteurs
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100,2)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100,2)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100,2)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100,2)

    nbreBanque = Offre.objects.filter(secteur__contains="Banque").count()
    pourcentageBanque = round(nbreBanque/nbreOffres*100,2)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100,2)

    return render(request,"pays.html",{'offres': offres, 'pays': pays,'no': nbreOffres,'np': nbrePays,'ns': ns,'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'banque': pourcentageBanque,'env': pourcentageEnv})

#page filtre par secteur
def secteur(request,s):
    #la liste des offres filtrée par secteur
    offres_list = Offre.objects.filter(secteur__contains=s)
    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    secteur = s

    #les statistiques générales
    nbreOffres = offres_list.count()

    nbrePays = offres_list.values_list('pays').distinct().count() - 1

    nbreSecteur = 1

    #les statistiques concernant les types de contrats
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(secteur__contains=s).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(secteur__contains=s).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(secteur__contains=s).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(secteur__contains=s).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(secteur__contains=s).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #les statistiques concernant les secteurs
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreBanque = Offre.objects.filter(secteur__contains="Banque").count()
    pourcentageBanque = round(nbreBanque/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"secteur.html",{'offres': offres, 'secteur': secteur,'no': nbreOffres,'np': nbrePays,'ns': nbreSecteur, 'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'banque': pourcentageBanque,'env': pourcentageEnv})

#page filtre par pays et secteur
def pays_secteur(request,p,s):
    #la liste des offres filtrée par pays et secteur
    offres_list = Offre.objects.filter(secteur__contains=s,pays=p)
    paginator = Paginator(offres_list,10)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    secteur = s
    pays = p

    #les statistiques générales
    nbreOffres = offres_list.count()

    nbrePays = 1

    nbreSecteur = 1

    #les statistiques concernant les types de contrats
    nbreCDI = Offre.objects.filter(typeContrat__contains="CDI").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageCDI = round(nbreCDI/nbreOffres*100)

    nbreCDD = Offre.objects.filter(typeContrat__contains="CDD").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageCDD = round(nbreCDD/nbreOffres*100)

    nbreStage = Offre.objects.filter(typeContrat__contains="Stage").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageStage = round(nbreStage/nbreOffres*100)

    nbreAlternance = Offre.objects.filter(typeContrat__contains="Alternance").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageAlternance = round(nbreAlternance/nbreOffres*100)

    nbreFreelance = Offre.objects.filter(typeContrat__contains="Freelance").filter(secteur__contains=s).filter(pays=pays).count()
    pourcentageFreelance = round(nbreCDI/nbreOffres*100)


    #les statistiques concernant les secteurs
    nbreInf = Offre.objects.filter(secteur__contains="Informatique").count()
    pourcentageInf = round(nbreInf/nbreOffres*100)

    nbreVente = Offre.objects.filter(secteur__contains="Vente").count()
    pourcentageVente = round(nbreVente/nbreOffres*100)

    nbreBatiment = Offre.objects.filter(secteur__contains="Batiment").count()
    pourcentageBatiment = round(nbreBatiment/nbreOffres*100)

    nbreInd = Offre.objects.filter(secteur__contains="Electrique").count()
    pourcentageInd = round(nbreInd/nbreOffres*100)

    nbreBanque = Offre.objects.filter(secteur__contains="Banque").count()
    pourcentageBanque = round(nbreBanque/nbreOffres*100)

    nbrEnv = Offre.objects.filter(secteur__contains="Agriculture").count()
    pourcentageEnv = round(nbrEnv/nbreOffres*100)

    return render(request,"pays_secteur.html",{'offres': offres, 'secteur': secteur, 'pays': p,'no': nbreOffres,'np': nbrePays,'ns': nbreSecteur,'cdi': pourcentageCDI, 'cdd': pourcentageCDD,'stage': pourcentageStage,'alternance': pourcentageAlternance,'freelance': pourcentageFreelance,'inf': pourcentageInf,'vente': pourcentageVente,'batiment': pourcentageBatiment,'industrie': pourcentageInd,'banque': pourcentageBanque,'env': pourcentageEnv})


#barre de recherche
def rechercheInt(request):

    offres_list = Offre.objects.filter(intitule__contains=request.POST.get('intitule'))
    paginator = Paginator(offres_list,5)

    page = request.GET.get('page')
    offres = paginator.get_page(page)

    return render(request,"recherche.html",{'offres': offres})
