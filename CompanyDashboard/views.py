#importation des modules
from django.shortcuts import render
from .models import Entreprise
from django.db.models import Count

#nombre d'entreprise par secteur au pays selectionné
def entrepSenegal(request):
    
    pays=""

    if request.method == 'POST':
        if request.POST.get('Nigeria') is not None:
            pays='Nigeria'
        if request.POST.get('Senegal') is not None:
            pays='Senegal'
        if request.POST.get('Maroc') is not None:
            pays='Maroc'
        if request.POST.get('algerie') is not None:
            pays='algerie'

    secteurs = Entreprise.objects.filter(pays__contains=pays)
    Bs= secteurs.values ('secteur','pays').annotate(numb_ent=Count('secteur')).order_by()
    barlabels = []
    bardata = []
    for B in Bs:
        bardata.append(B['numb_ent'])
        barlabels.append(B['secteur'])

    return render(request, 'Dashboard/Senegal.html', {'secteurs': secteurs, 'Bs':Bs,'barlabels':barlabels ,'bardata':bardata})




   

#tableau d'entreprises en fonction du nom,du pays,de la region de l'adresse et du secteur 
def table(request):
    entreprises=Entreprise.objects.filter()
    return render(request, 'Dashboard/tables.html', {'entreprises': entreprises})

#Nombre d'entreprise par secteur par pays
def piesec(request):
    secteurs = Entreprise.objects.all()
    Bs= secteurs.values ('secteur').annotate(numb_ent=Count('secteur')).order_by()
    labels = []
    data = []
    for B in Bs:
        data.append(B['numb_ent'])
        labels.append(B['secteur'])


    

    
    

    return render(request, 'Dashboard/piesec.html',{'secteurs' : secteurs, 'Bs':Bs, 'data':data, 'labels':labels})

#Nombre d'entreprise par secteur par pays
def piepays(request):
    secteurs = Entreprise.objects.all()
    Bs= secteurs.values ('secteur').annotate(numb_ent=Count('secteur')).order_by()
    labels = []
    data = []
    for B in Bs:
        data.append(B['numb_ent'])
        labels.append(B['secteur'])
    return render(request, 'Dashboard/piesec.html',{'secteurs' : secteurs, 'Bs':Bs, 'data':data, 'labels':labels})



